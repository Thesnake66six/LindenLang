from collections import deque
from decimal import DivisionByZero
import sys
import turtle

characters = {}
rules = {}
axiom = ""
depth = 0

filepath = input("Enter relative filepath of a .lind file or leave blank to write into the terminal.\n")

input_lines = []

depth_override = -1.0
if len(filepath.strip()) > 0:
    f = open(filepath.strip(), 'r')
    for line in f:
        input_lines.append(line)

    depth_override = int(input("Enter depth override. Leave blank for the file-specified depth\n").strip()) 
        
else:
    input_lines[0]
    print("Enter code here")
    input_lines = sys.stdin.readlines()

for line in input_lines:
    line = line.replace(' ', '')
    line = line.replace('\n', '')

    if line.startswith('#'):
        continue

    elif ':' in line:
        line = line.split(":")
        characters.update({line[0]: line[1]})
    
    elif "->" in line:
        line = line.split("->")
        rules.update({line[0]: line[1]})

    elif "→" in line:
        line = line.split("→")
        rules.update({line[0]: line[1]})
    
    elif '"' in line or "'" in line:
        line.replace('"', '')
        line.replace("'", "")

        axiom += line

    elif line.isnumeric():
        depth = int(line)

if depth_override != -1:
    depth = depth_override

for i in range(depth):
    new_axiom = ""

    for char in axiom:
        try:
            new_axiom += rules[char]
        except:
            new_axiom += char
    
    axiom = new_axiom

turtle.tracer(False)

screen = turtle.Screen()
elizabeth = turtle.Turtle()
elizabeth.hideturtle()

stack = deque()

for char in axiom:
    if char in characters.keys():
        todo = characters[char]
    else:
        continue
    
    if todo == 'SAVE':
        stack.append((elizabeth.pos(), elizabeth.heading()))
    
    elif todo == 'LOAD':
        place = stack.pop()
        elizabeth.setpos(place[0])
        elizabeth.setheading(place[1])

    elif todo[:2] == ">>":
        todo = todo.replace('>', '')
        todo = todo.split("^")
        try:
            elizabeth.forward(float(todo[0]) / (float(todo[1]) ** depth))
        except DivisionByZero:
            elizabeth.forward(float(todo[0]))
        except IndexError:
            elizabeth.forward(float(todo[0]) / depth)

    elif todo[0] == '>':
        todo = todo.replace('>', '')
        elizabeth.forward(float(todo))

    elif todo[0] == '*':
        todo = todo.replace('*', '')
        elizabeth.right(float(todo))

turtle.update()
turtle.done()